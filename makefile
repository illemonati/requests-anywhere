SRC_DIR := ./cmd/requests-anywhere-server/
BUILD_DIR := ./build/



PLATFORMS=darwin linux windows
ARCHITECTURES=386 amd64
NAME=requests-anywhere-server

ifeq ($(OS),Windows_NT)     # is Windows_NT on XP, 2000, 7, Vista, 10...
    setenv := set
else
    setenv := export
endif

default: all

build:
	go build ${LDFLAGS} -o ${BINARY}

all:
	$(foreach GOOS, $(PLATFORMS),\
		$(foreach GOARCH, $(ARCHITECTURES), $(shell $(setenv) GOOS=$(GOOS); $(setenv) GOARCH=$(GOARCH); go build --trimpath -v -ldflags "-s -w" -o $(BUILD_DIR)/$(GOOS)/$(NAME)-$(GOOS)-$(GOARCH)$(if $(filter windows , $(GOOS)),.exe) $(SRC_DIR))))

clean:
	rm -r build/
