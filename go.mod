module gitlab.com/illemonati/requests-anywhere

go 1.14

require (
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751 // indirect
	github.com/alecthomas/units v0.0.0-20190924025748-f65c72e2690d // indirect
	github.com/gin-gonic/autotls v0.0.0-20200314141124-cc69476aef2a
	github.com/gin-gonic/gin v1.6.2
	github.com/imroc/req v0.3.0
	github.com/rakyll/statik v0.1.7
	gopkg.in/alecthomas/kingpin.v2 v2.2.6
)
