# Requests Anywhere

inspired by cors anywhere, basically just a proxy for your http requests

## install

1) make sure go is installed

2) run these commands

```sh
git clone https://gitlab.com/illemonati/requests-anywhere.git

cd requests-anywhere

make
```

check the build folder for your version of the executable

## usage 

```$xslt
usage: requests-anywhere [<flags>]

Flags:
      --help            Show context-sensitive help (also try --help-long and --help-man).
  -p, --port=80         port to run on
  -h, --host="0.0.0.0"  host to run on
  -d, --debug           enable debug mode
  -t, --tls=""          space separated list of domains for autotls
```
