package server

import (
	"github.com/gin-gonic/autotls"
	"github.com/gin-gonic/gin"
	"github.com/imroc/req"
	StatikFS "github.com/rakyll/statik/fs"
	_ "gitlab.com/illemonati/requests-anywhere/server/statik"
	"log"
	"net/http"
)

// Server : The Main Server For Requests Anywhere
type Server struct {
	ginEngine *gin.Engine
	statikFs http.FileSystem
}

// New : Creates a new Server
func New(debug bool) *Server {
	if !debug {
		gin.SetMode(gin.ReleaseMode)
	} else {
		gin.SetMode(gin.DebugMode)
	}
	ginEngine := gin.Default()
	statikFS, err := StatikFS.New()
	if err != nil {
		log.Fatal(err)
	}
	server := new(Server)
	server.ginEngine = ginEngine
	server.statikFs = statikFS
	server.Setup()
	return server
}


// Setup : Sets up the paths for the server
func (s *Server) Setup() {
	s.setUpHelpPage()
	s.ginEngine.GET("/request/", func(c *gin.Context) {
		url := c.Query("url")

		reqHeaders := c.Request.Header
		resp, err := req.Get(url, reqHeaders, c.Request.Body, c.Request)
		processResponse(c, resp, err)
	})
	s.ginEngine.POST("/request/", func(c *gin.Context) {
		url := c.Query("url")

		reqHeaders := c.Request.Header
		resp, err := req.Post(url, reqHeaders, c.Request.Body, c.Request)
		processResponse(c, resp, err)
	})
	s.ginEngine.PUT("/request/", func(c *gin.Context) {
		url := c.Query("url")

		reqHeaders := c.Request.Header
		resp, err := req.Put(url, reqHeaders, c.Request.Body, c.Request)
		processResponse(c, resp, err)
	})
	s.ginEngine.PATCH("/request/", func(c *gin.Context) {
		url := c.Query("url")

		reqHeaders := c.Request.Header
		resp, err := req.Patch(url, reqHeaders, c.Request.Body, c.Request)
		processResponse(c, resp, err)
	})
	s.ginEngine.DELETE("/request/", func(c *gin.Context) {
		url := c.Query("url")

		reqHeaders := c.Request.Header
		resp, err := req.Delete(url, reqHeaders, c.Request.Body, c.Request)
		processResponse(c, resp, err)
	})
	s.ginEngine.OPTIONS("/request/", func(c *gin.Context) {
		url := c.Query("url")

		reqHeaders := c.Request.Header
		resp, err := req.Options(url, reqHeaders, c.Request.Body, c.Request)
		processResponse(c, resp, err)
	})
	s.ginEngine.HEAD("/request/", func(c *gin.Context) {
		url := c.Query("url")

		reqHeaders := c.Request.Header
		resp, err := req.Head(url, reqHeaders, c.Request.Body, c.Request)
		processResponse(c, resp, err)
	})
}

func processResponse(c *gin.Context, resp *req.Resp, err error) {
	if err != nil {
		c.String(http.StatusInternalServerError, "Big Wrong")
		log.Println(err)
		return
	}
	response := resp.Response()
	contentType := response.Header.Get("Content-Type")

	extraHeaders := map[string]string {}

	c.DataFromReader(response.StatusCode, response.ContentLength, contentType, response.Body, extraHeaders)
}


func (s *Server) setUpHelpPage() {
	s.ginEngine.GET("/", func(c *gin.Context) {
		c.FileFromFS("/helpPage.html", s.statikFs)
		return
	})
	s.ginEngine.GET("/helpPage.css", func(c *gin.Context) {
		c.FileFromFS("/helpPage.css", s.statikFs)
		return
	})
	s.ginEngine.GET("/helpPage.js", func(c *gin.Context) {
		c.FileFromFS("/helpPage.js", s.statikFs)
		return
	})
}


// Runs : Runs a Server
func (s *Server) Run(addrString string) (err error) {
	err = s.ginEngine.Run(addrString)
	return nil
}

// Runs : Runs a Server with autotls from letsencrypt
func (s *Server) RunWithAutoTls(addrString string, domains ...string) (err error) {
	err = autotls.Run(s.ginEngine, domains...)
	if err != nil {
		return
	}
	err = s.ginEngine.Run(addrString)
	if err != nil {
		return
	}
	return nil
}
