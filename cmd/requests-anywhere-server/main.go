package main

import (
	"fmt"
	server "gitlab.com/illemonati/requests-anywhere/server"
	"gopkg.in/alecthomas/kingpin.v2"
	"log"
	"strings"
)

var (
	port 		= kingpin.Flag("port", "port to run on").Default("80").Short('p').Int()
	host 		= kingpin.Flag("host", "host to run on").Default("0.0.0.0").Short('h').String()
	debug		= kingpin.Flag("debug", "enable debug mode").Default("false").Short('d').Bool()
	tlsDomains 	= kingpin.Flag("tls", "space separated list of domains for autotls").Short('t').Default("").String()
)


func main() {
	kingpin.Parse()
	addrString := fmt.Sprintf("%s:%d", *host, *port)
	var domains []string
	if len(*tlsDomains) > 0 {
		for _, d := range strings.Split(*tlsDomains, " ") {
			domains = append(domains, d)
		}
	}
	mainServer := server.New(*debug)
	var err error
	if len(domains) == 0 {
		err = mainServer.Run(addrString)
	} else {
		err = mainServer.RunWithAutoTls(addrString, domains...)
	}
	if err != nil {
		log.Fatal(err)
	}
}
